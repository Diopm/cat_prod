package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.entities.Produit;

public class ProduitDaoImpl implements IProduitDao {

	@Override
	public Produit save(Produit p)  {
		
		Connection connection=SingletonConnection.getConnection();
		try {
		
			PreparedStatement ps=connection.prepareStatement("INSERT INTO produits(designation,prix,quantite) VALUES(?,?,?)");
		    ps.setString(1, p.getDesignation());
		    ps.setDouble(2, p.getPrix());
		    ps.setInt(3, p.getQuantite());
		    ps.executeUpdate();
		    
		    PreparedStatement ps2=connection.prepareStatement("SELECT MAX(ID_produit) AS MAX_ID_produit FROM produits");
		    ResultSet rs=ps2.executeQuery();
		    if(rs.next()) 
		    {
		    	p.setID_produit(rs.getLong("MAX_ID_produit"));
		    }
		    ps.close();
		}catch(SQLException e) {e.printStackTrace();}
		return p;
	}

	@Override
	public List<Produit> produitsParMC(String mc) {
		
		List<Produit> produits = new ArrayList<Produit>();
		
		Connection connection=SingletonConnection.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM produits WHERE designation LIKE ?");
			ps.setString(1, mc);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Produit p=new Produit();
				p.setID_produit(rs.getLong("ID_produit"));
				p.setDesignation(rs.getString("designation"));
				p.setPrix(rs.getDouble("prix"));
				p.setQuantite(rs.getInt("quantite"));
				produits.add(p);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return produits;
	}

	@Override
	public Produit findOne(Long id) {
		
		Produit produit=null;
		
		Connection connection=SingletonConnection.getConnection();
		try {
			 PreparedStatement ps= connection.prepareStatement("SELECT * FROM produits WHERE ID_produit=?");
			 ps.setLong(1, id);
			 ResultSet rs = ps.executeQuery();
			 if(rs.next()){
			   produit=new Produit();
			   produit.setID_produit(rs.getLong("ID_produit"));
			   produit.setDesignation(rs.getString("DESIGNATION"));
			   produit.setPrix(rs.getDouble("PRIX"));
			   produit.setQuantite(rs.getInt("QUANTITE"));
			}
			 
			
		}catch(SQLException e) {e.printStackTrace();}
		return produit;
	}

	@Override
	public Produit update(Produit p) {
		
		Connection connection=SingletonConnection.getConnection();
		try {
			PreparedStatement ps = connection.prepareStatement("UPDATE produits SET designation=?, prix=?, quantite=? WHERE ID_produit=?");
			ps.setString(1, p.getDesignation());
			ps.setDouble(2, p.getPrix());
			ps.setInt(3, p.getQuantite());
			ps.setLong(4, p.getID_produit());
			ps.executeUpdate();
			ps.close();
			
		}catch(SQLException e) {e.printStackTrace();}
		return p;
	}

	@Override
	public void deleteProduit(Long id) {
		Connection connection = SingletonConnection.getConnection();
		try {
			PreparedStatement ps =connection.prepareStatement("DELETE FROM produits WHERE ID_produit=?");
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();
		}catch(SQLException e) {e.printStackTrace();}
		
	}
	public void init() {
		   System.out.println("initialisation Spring...");
	   }
}
