package dao;

import java.util.List;

import metier.entities.Produit;

public class TestDao {

	public static void main(String[] args) {
		
		ProduitDaoImpl dao= new ProduitDaoImpl();
		Produit p1=dao.save(new Produit("HP 6554",1000,45));
		Produit p2=dao.save(new Produit("Epson 442",200,15));
		Produit p3=dao.save(new Produit("Samsung",1200,5));
		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println("Chercher les produit");
		List<Produit> prods=dao.produitsParMC("%H%");
		for(Produit p:prods) {
			System.out.println(p.toString());
		}
		
	}

}
