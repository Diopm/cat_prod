package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import dao.IProduitDao;
import dao.ProduitDaoImpl;
import metier.entities.Produit;

//@WebServlet(name="cs",urlPatterns = { "*.php"})
public class ControleurServlet extends HttpServlet{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	  * couplage faible
	  */
	private IProduitDao metier;
	
	public void init() throws ServletException{
		/*
		 * couplabe fort
		    
		    metier = new ProduitDaoImpl();
		    On va demande a spring de creer un bean dont le id=dao
		    Pour cela on recupere le contexte de spring
		 */
		ApplicationContext springContext =  WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
		//metier = (IProduitDao) springContext.getBean("dao");
		metier = springContext.getBean(IProduitDao.class);
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * On cherche a savoir le path utiliser par le client
		 */
		String path=request.getServletPath();
        
        if(path.equals("/index.php")) {
			
			request.getRequestDispatcher("produits.jsp").forward(request, response);
		}
        else if(path.equals("/chercher.php")) {
			
			String motCle = request.getParameter("motCle");
		    ProduitModel model = new ProduitModel();
		    model.setMotCle(motCle);
		    List<Produit> produits= metier.produitsParMC("%"+motCle+"%");
		    model.setProduits(produits);
		    request.setAttribute("model", model);
		    
		    request.getRequestDispatcher("produits.jsp").forward(request, response);
		}
        else if(path.equals("/saisie.php")) {
			
			request.getRequestDispatcher("saisieProduit.jsp").forward(request, response);
		}
        else if(path.equals("/saveProduit.php") && (request.getMethod().equals("POST"))) {
			
     	   String design = request.getParameter("designation");
 			double prix = Double.parseDouble(request.getParameter("prix"));
 			int quantite = Integer.parseInt(request.getParameter("quantite"));
 			
 			Produit p = metier.save(new Produit(design,prix,quantite));
 			request.setAttribute("produit", p);
 			request.getRequestDispatcher("confirmation.jsp").forward(request, response);
 		}
        else if(path.equals("/supprimer.php")) {
     	   Long id= Long.parseLong(request.getParameter("id"));
     	   metier.deleteProduit(id);
     	  // request.getRequestDispatcher("produits.jsp").forward(request, response);
     	   response.sendRedirect("chercher.php?motCle=");
        }
        else if(path.equals("/editer.php")) {
     	   Long id= Long.parseLong(request.getParameter("id"));
     	   Produit p=metier.findOne(id);
     	   request.setAttribute("produit", p);
     	   request.getRequestDispatcher("editerProduit.jsp").forward(request, response);
     	   
        }
        else if(path.equals("/updateProduit.php") && (request.getMethod().equals("POST"))) {
     	   Long id= Long.parseLong(request.getParameter("id"));
     	   String design = request.getParameter("designation");
 		   double prix = Double.parseDouble(request.getParameter("prix"));
 		   int quantite = Integer.parseInt(request.getParameter("quantite"));
 			
 			Produit p = new Produit(design, prix, quantite);
 			
 			p.setID_produit(id);
 			metier.update(p);
 			request.setAttribute("produit", p);
 			request.getRequestDispatcher("confirmation.jsp").forward(request, response);
 		}
 		else {
 			response.sendError(404);
 			
 		}
        
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
}
