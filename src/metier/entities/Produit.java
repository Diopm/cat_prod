package metier.entities;

import java.io.Serializable;

public class Produit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long ID_produit;
	private String designation;
	private double prix;
	private int quantite;
	
	public Produit() {
		super();
	}

	public Produit(String designation, double prix, int quantite) {
		super();
		this.designation = designation;
		this.prix = prix;
		this.quantite = quantite;
	}

	public Long getID_produit() {
		return ID_produit;
	}

	public void setID_produit(Long iD_produit) {
		ID_produit = iD_produit;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	@Override
	public String toString() {
		return "Produit [ID_produit=" + ID_produit + ", designation=" + designation + ", prix=" + prix + ", quantite="
				+ quantite + "]";
	}
	
	
	
	
}
