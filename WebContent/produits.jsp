<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<%@include file="header.jsp" %>
<div class="container-fluid col-md-6 offset-md-3">
    <div class="card ">
      <div class="card-header text-center"> Recherche des produits</div>
      <div class="card-body">
       <form action="chercher.php" method="get">
         <label>Mot cle</label>
         <input type="text"name="motCle" value="${model.motCle }" />
         <button class="btn btn-default" type="submit">Chercher</button>
       </form>
       <table class="table table">
         <tr>
           <th>ID</th>
           <th>DESIGNATION</th>
           <th>PRIX</th>
           <th>QUANTITE</th>
         </tr>
         <c:forEach items ="${model.produits }" var="p">
          <tr>
           <td>${p.ID_produit }</td>
           <td>${p.designation }</td>
           <td>${p.prix }</td>
           <td>${p.quantite }</td>
           <td><a onClick="return confirm('Etes vous sure ')" href="supprimer.php?id=${p.ID_produit}">Supprimer</a></td>
           <td><a href="editer.php?id=${p.ID_produit}">Editer</a></td>
           <td></td>
          </tr>
           
         </c:forEach>
       </table>
      </div>
    </div>
 
</div>
</body>
</html>