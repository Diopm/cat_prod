<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Produits</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<%@include file="header.jsp" %>
<div class="container-fluid col-md-6 offset-md-3">
  
    <div class="card ">
      <div class="card-header text-center"> Confirmation</div>
      <div class="card-body">
       <div class="form-group">
         <label>ID:</label>
         <label>${produit.ID_produit}</label>
       </div>
       <div class="form-group">
         <label>Designation:</label>
         <label>${produit.designation }</label>
       </div>
       <div class="form-group">
         <label>Prix:</label>
         <label>${produit.prix }</label>
       </div>
       <div class="form-group">
         <label>Quantite:</label>
         <label>${produit.quantite }</label>
       </div>
       <div class="form-group">
         <a href="chercher.php?motCle=">Retour</a>
       </div>
      </div>
      
    </div>
     
</div>
</body>
</html>