<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Produits</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<%@include file="header.jsp" %>
<div class="container-fluid col-md-6 offset-md-3">
  
    <div class="card ">
      <div class="card-header text-center"> Saisie d'un Produits</div>
      <div class="card-body">
      <form method="post" action="saveProduit.php">
       <div class="form-group">
        <label for="formGroupExample">Designation</label>
        <input type="text" class="form-control" name="designation" placeholder="Designation">
       </div>
       <div class="form-group">
        <label for="formGroupExample">Prix</label>
        <input type="text" class="form-control" name="prix" placeholder="Prix">
       </div>
       <div class="form-group">
        <label for="formGroupExample">Quantite</label>
        <input type="text" class="form-control" name="quantite" placeholder="Quantite">
       </div>
       <div>
        <button type="submit" class="btn btn-default">Save</button>
       </div>
      </form>
      </div>
    </div>
 
</div>
</body>
</html>